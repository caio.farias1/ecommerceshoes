import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  usuarioEstaLogado: localStorage.getItem('token') ? true : false,
  usuarioLogado: localStorage.getItem('usuario') ? JSON.parse(localStorage.getItem('usuario')) : {},
};

export const usuarioSlice = createSlice({
  name: 'usuario',
  initialState,
  reducers: {
    salvarUsuario: (state, action) => {
            state.usuarioLogado = action.payload;
            state.usuarioEstaLogado = true;
        },
    apagarUsuario: (state) => {
      state = initialState;
    }
    },
});

export const { salvarUsuario, apagarUsuario } = usuarioSlice.actions;

export default usuarioSlice.reducer;
