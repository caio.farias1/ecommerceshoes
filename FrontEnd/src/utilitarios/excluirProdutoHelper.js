import axios from "../axios/axios";

const excluirProdutoHelper = async id => {
  try {
    const respota = await axios.delete( `cardapio/${ id }` );
    return true;
  } catch(e) {
    return false;
  }

};

export default excluirProdutoHelper;
