import axios from "../axios/axios";

const editarProdutoHelper = async (id, produtoEditado) => {
    try {
        const resposta = axios.put(`cardapio/${ id }`, produtoEditado)
        return true
    } catch(e){

    }
};

export default editarProdutoHelper;