import axios from "../axios/axios"

const logarHelper = async ( usuario, senha) => {
    try {
        const dados = { usuario, senha };
        const resultado = await axios.post( 'auth', dados )

        localStorage.setItem('token', resultado.data.token )
        localStorage.setItem('usuario', JSON.stringify(resultado.data.usuario) )


        return {
            sucesso: true,
            usuario: resultado.data.usuario
        };
    } catch (e) {
        return {
            sucesso: false,
            usuario: null
        };
    }
}

export default logarHelper;