import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Cabecalho from './features/Cabecalho/Cabecalho';
import Rodape from './features/Rodape/Rodape';
import BarraLateral from './features/BarraLateral/BarraLateral';
import ConteudoCentral from './features/ConteudoCentral/ConteudoCentral';
import axios from './axios/axios';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { salvarProdutos } from './store/slices/produtosSlice';
import { salvarCategorias} from './store/slices/categoriasSlice';
import Login from './features/Login/Login';
import Logout from './features/Login/Logout';
import Cadastro from './features/Produtos/Cadastro';
import Edicao from './features/Produtos/Edicao';
import ProdutoDetalhe from './features/ConteudoCentral/ProdutoDetalhe/ProdutoDetalhe';


const RotaPrivada = ( {component:Component, ...props} ) => { 
  const usuarioEstaLogado = useSelector(state => state.usuario.usuarioEstaLogado);
  const usuarioLogado = useSelector(state => state.usuario.usuarioLogado);

  if(usuarioEstaLogado){
    if((props.validarAdmin && usuarioLogado.tipo === '1')){
      return <Route {...props}><Component/></Route>;
    } else if (!props.validarAdmin) {
      return <Route {...props}><Component/></Route>;
    } else {
      alert('Você não tem permissão!')
    }
  }
  return <Redirect to='/tela-login'/>
}


const Ecommerce = () => {

  const dispatch = useDispatch();
  const usuarioEstaLogado = useSelector(state => state.usuario.usuarioEstaLogado);
  const usuarioLogado = useSelector(state => state.usuario.usuarioLogado);
  const atualizarListaCompleta = useSelector(state => state.produtos.atualizarListaCompleta);

  useEffect( async () => {

    try{
      const resultado = await axios.get('cardapio');
      console.log(resultado.data)
      dispatch(salvarProdutos(resultado.data));
      dispatch(salvarCategorias(resultado.data));

    } catch (e){
      alert("Houve um erro de comunicaçào com a API");
    }
  }, [atualizarListaCompleta]);

  return <>
    
  <BrowserRouter>
    <div id='ecommerce'>
      <Cabecalho />
      <div className="container">
        <Switch>
          <Route path='/' exact>
            <div className="row">
              <div className="col-10">
                <ConteudoCentral />
              </div>
            </div>
          </Route>

          <Route path='/tela-login'>
            <Login/>
          </Route>
          <Route path='/tela-logout'>
            <Logout/>
          </Route>

          <Route path='/produtos/:id'>
            <ProdutoDetalhe/>
          </Route>

          <RotaPrivada validarAdmin={true} component={ Cadastro } path='/cadastro-produto'/>
          <RotaPrivada validarAdmin={true} component={ Edicao } path='/edicao-produto/:id'/>




        </Switch>
      </div>
      <Rodape />
    </div>
  </BrowserRouter>
</>;

};

export default Ecommerce;