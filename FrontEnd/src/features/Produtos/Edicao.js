import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useState, useEffect } from 'react';
import editarProdutoHelper from '../../utilitarios/editarProdutoHelper';
import { useHistory, useParams } from 'react-router-dom';
import { atualizarListaCompletaProdutos } from '../../store/slices/produtosSlice';
import pegarProdutoHelper from '../../utilitarios/pegarProdutoHelper';

const Edicao = () => {
    const dispatch = useDispatch();
    const history = useHistory();

    const {id} = useParams();
    useEffect( async () => {
        const resultado = await pegarProdutoHelper(id);
        if(resultado.sucesso){
            const produto = resultado.produto;

            setTitulo(produto.produto);
            setPreco(produto.preco);
            setDescricao(produto.descricao); 
            setCategoria(produto.categoria); 
            setImagem(produto.imagem);
            setImagemDestaque(produto.imagem_destaque);
        }
    },[])

    const usuarioEstaLogado = useSelector(state => state.usuario.usuarioEstaLogado);
    const usuarioLogado = useSelector(state => state.usuario.usuarioLogado);
    const categorias = useSelector(state => state.categorias.lista);

    const [ titulo, setTitulo] = useState('');
    const [ preco, setPreco] = useState('');
    const [ descricao, setDescricao] = useState(''); 
    const [ categoria, setCategoria] = useState(''); 
    const [ imagem, setImagem] = useState('');
    const [ imagemDestaque, setImagemDestaque] = useState('');
    const editar = () => {
        const produtoEditado = {
            produto: titulo,
            preco: preco.replace(',', '.'),
            descricao: descricao,
            categoria: categoria,
            imagem: imagem,
            imagem_destaque: imagemDestaque
        };

        const resultado = editarProdutoHelper( id, produtoEditado );

        if (resultado ) {
            dispatch(atualizarListaCompletaProdutos());
            history.push( '/' );
        }else {
            alert('Houve um erro!')
        }


    }


    
    return <>
    <>
        <h1>Edição de Produto</h1>
        <form onSubmit = { e => e.preventDefault() }>
            <div className= 'cp-campo'>
                <label htmlFor= 'titulo'>Título</label>
                <input type='text' id='titulo' name='titulo' value={titulo} onChange = {e => setTitulo(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'preco'>Preçp</label>
                <input type='text' id='preco' name='preco' value={preco} onChange = {e => setPreco(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'descricao'>Descriçao</label>
                <input type='textarea' id='descricao' name='descricao' value={descricao} onChange = {e => setDescricao(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'categoria'>Categoria</label>
                    <select id='categoria' name='categoria' onChange = {e => setCategoria(e.target.value)}>
                        <option selected={categoria === null} disabled value='-1'>Selecione a categoria</option>
                        {categorias.map (item => <option value={item} selected={item === categoria}>
                            {item}
                        </option>)}; 
                    </select>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'imagem'>Imagem</label>
                <input type='text' id='imagem' name='imagem' value={imagem} onChange = {e => setImagem(e.target.value)}/>
                <img src={imagem}/>
            </div>
            
            <div className= 'cp-campo'>
                <label htmlFor= 'imagem-destaque'>Imagem de Destaque</label>
                <input type='text' id='imagem-destaque' name='imagem-destaque' value={imagemDestaque} onChange = {e => setImagemDestaque(e.target.value)}/>
                <img src={imagemDestaque}/>
            </div>
            <button className='btn btn-primary' onClick={() => editar()}>Editar</button>
        </form>
    </>
    </>;
}


export default Edicao;