import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useState } from 'react';
import { salvarCampoPesquisar } from '../../store/slices/produtosSlice';
import salvarProdutoHelper from '../../utilitarios/salvarProdutoHelper';
import { useHistory } from 'react-router-dom';
import { atualizarListaCompletaProdutos } from '../../store/slices/produtosSlice';

const Cadastro = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    const usuarioEstaLogado = useSelector(state => state.usuario.usuarioEstaLogado);
    const usuarioLogado = useSelector(state => state.usuario.usuarioLogado);
    const categorias = useSelector(state => state.categorias.lista);

    const [ titulo, setTitulo] = useState('');
    const [ preco, setPreco] = useState('');
    const [ descricao, setDescricao] = useState(''); 
    const [ categoria, setCategoria] = useState(''); 
    const [ imagem, setImagem] = useState('');
    const [ imagemDestaque, setImagemDestaque] = useState('');
    const salvar = async () => {
        const novoProduto = {
            produto: titulo,
            preco: preco.replace(',', '.'),
            descricao: descricao,
            categoria: categoria,
            imagem: imagem,
            imagem_destaque: imagemDestaque
        };

        const resultado = await salvarProdutoHelper( novoProduto );

        if ( resultado ) {
            dispatch(atualizarListaCompletaProdutos())
            history.push('/')
        } else {
            alert("Hove um erro!")
        }
    }


    
    return <>

    
    <>
        <h1>Cadastro Produto</h1>
        <form onSubmit = { e => e.preventDefault() }>
            <div className= 'cp-campo'>
                <label htmlFor= 'titulo'>Título</label>
                <input type='text' id='titulo' name='titulo' onChange = {e => setTitulo(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'preco'>Preçp</label>
                <input type='text' id='preco' name='preco' onChange = {e => setPreco(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'descricao'>Descriçao</label>
                <input type='textarea' id='descricao' name='descricao' onChange = {e => setDescricao(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'categoria'>Categoria</label>
                    <select id='categoria' name='categoria' onChange = {e => setCategoria(e.target.value)}>
                        <option selected disabled value='-1'>Selecione a categoria</option>
                        {categorias.map (categoria => <option value={categoria}>
                            {categoria}
                        </option>)}; 
                    </select>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'imagem'>Imagem</label>
                <input type='text' id='imagem' name='imagem' onChange = {e => setImagem(e.target.value)}/>
            </div>
            <div className= 'cp-campo'>
                <label htmlFor= 'imagem-destaque'>Imagem de Destaque</label>
                <input type='text' id='imagem-destaque' name='imagem-destaque' onChange = {e => setImagemDestaque(e.target.value)}/>
            </div>
            <button className='btn btn-primary' onClick={() => salvar()}>Salvar</button>
        </form>
    </>


    </>;
}


export default Cadastro;