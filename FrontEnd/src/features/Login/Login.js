import React , { useState }from 'react';
import { useDispatch } from 'react-redux';
import logarHelper from '../../utilitarios/logarHelper';
import './Login.css'
import { salvarUsuario } from '../../store/slices/usuarioSlice';
import { useHistory } from 'react-router-dom';

const Login = () => {

    const dispatch = useDispatch();
    const history = useHistory();

    const [usuario, setUsuario] = useState('');
    const [senha, setSenha] = useState('');

    const logar = async () => {

        const resultado = await logarHelper(usuario, senha);
        console.log( resultado );
        // dispatch ( salvarUsuario() );

        if(resultado.sucesso){
            //Salva no redux as informações do usuário logado
            dispatch( salvarUsuario( resultado.usuario ));
            //direciona o usuario para a home
            history.push('/');
        } else {
            alert( 'Usuario não autenticado.'); 
        }
     }

    return <>

        <h2>Tela de Login</h2>
        <form onSubmit= { e => e.preventDefault() }>
            <div className='l-campo'>
                <label htmlFor='usuario'>Usuário: </label>
                <input type='text' id= 'usuario' name='usuario' onChange={ e => setUsuario(e.target.value) } />
            </div>
            <div className='l-campo'>
                <label htmlFor='senha'>Senha: </label>
                <input type='password' id='senha' name='senha' onChange={ e => setSenha(e.target.value) }/>
            </div>
            <button onClick={ () => logar() }>Entrar</button>
        </form>

    </>
};

export default Login;