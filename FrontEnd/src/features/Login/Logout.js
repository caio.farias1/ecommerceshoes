import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { apagarUsuario } from '../../store/slices/usuarioSlice';

const Logout = () => {

const history = useHistory();  
const dispatch = useDispatch();

const logout = () =>{
    // apagar token localstorage
    localStorage.removeItem('token');
    localStorage.removeItem('usuario'); 
 
    // apagar infos do usuário do Redux
    dispatch( apagarUsuario() );
    // direcionar o usu;ario pata tela inicial
    history.push('/');
}

    return <>

        <h2>Tela de Logout   </h2>
        <h5>Você tem certeza que deseja sair?</h5>
        <div>
            <button className="btn btn-primary"
            onClick = { logout }
            >Sim</button>
            <button className="btn btn-danger" onClick={ () => history.push('/') }>Não</button>
        </div>
    </>
};

export default Logout;