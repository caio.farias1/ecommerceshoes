import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import MenuPrincipal from './MenuPrincipal/MenuPrincipal';


const Cabecalho = () => {

  const produtos = useSelector( state => state.produtos.listaCompleta);
  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado);

  const style = {
    'span': {
      fontWeight: 'bold'
    }
  };

  return <>
    <header>

      <div className="container p-3">
        <div className="row">

          <div className="col-12 col-sm-8 col-md-10">
            <div>
            <Link to ='/'>
              <img 
                src='/imagens/logo-infnet.png' 
                alt='Logo' 
                title='Logo do Infnet.' />
            </Link>
            </div>
          </div>

          <div className="col-12 col-sm-4 col-md-2">
          {usuarioEstaLogado ? 
            <div>
              Seja bem-vindo, <span style={ styleMedia.span}> { usuarioLogado.nome } </span>.
            </div> : null } 
            <div>
              { usuarioEstaLogado ? 
              <Link to='/tela-logout'>
                <i className="fas fa-user"></i> Logout
              </Link>
              :
              <Link to='/tela-login'>
                <i className="far fa-user"></i> Login
              </Link>
              }
            </div>
          </div>

        </div>
      </div>


    <div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel">

        <div className="carousel-inner">

            { produtos.map ( (produto, indice ) => {
                return (                  
                    <div className={`carousel-item ${ indice === 0 ? 'active' : ''}` } >
                        <img src={produto.imagem_destaque} className="d-block w-100 img-destaque" alt={produto.produto}></img>
                        <div className="carousel-caption d-none d-md-block">
                        </div>
                    </div>
            )
            } ) }
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
      

      <MenuPrincipal />
    </header>
  </>;

};

export default Cabecalho;