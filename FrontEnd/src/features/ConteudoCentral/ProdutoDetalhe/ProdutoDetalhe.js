import React from 'react';
import { useState, useEffect } from 'react';
import { NavItem } from 'react-bootstrap';
import { useParams, useHistory } from 'react-router-dom';
import pegarProdutoHelper from '../../../utilitarios/pegarProdutoHelper';
import './ProdutoDetalhe.css';

const ProdutoDetalhe = () => {

    const [produto, setProduto] = useState({});
    const history = useHistory();
    
    const {id} = useParams();
    useEffect( () => {
        pegarProdutoHelper(id).then((result)=>{
            setProduto(result.produto);
        }).catch(()=>{
            alert('Houve um erro!');
        }) 
        
    },[])

  return <>
    <div id="produto">
        <img className="produto-img" src={produto.imagem}></img>
        <div className="produto-detalhe">
            <h1>{produto.produto}</h1>
            <h4>{produto.categoria}</h4>
            <p>{produto.descricao}</p>
            <p className="preco">R${produto.preco}</p>
            <button className='btn btn-primary'onClick={()=> history.goBack()}>Voltar</button>
        </div>
    </div>
    
  </>;

};

export default ProdutoDetalhe;