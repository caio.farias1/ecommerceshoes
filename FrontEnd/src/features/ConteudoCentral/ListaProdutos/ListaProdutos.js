import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import {NENHUMA_CATEGORIA_SELECIONADA, atualizarListaCompletaProdutos} from '../../../store/slices/produtosSlice'
import './ListaProdutos.css' ;
import excluirProdutoHelper from '../../../utilitarios/excluirProdutoHelper'


const ListaProdutos = () => {

  const lista = useSelector( state => state.produtos.lista );
  const categoriaSelecionada = useSelector( state => state.produtos.categoriaSelecionada );
  const usuarioEstaLogado = useSelector( state => state.usuario.usuarioEstaLogado );
  const usuarioLogado = useSelector( state => state.usuario.usuarioLogado );
  const [ produtoExclusao, setProdutoExclusao] = useState(null);

  const dispatch = useDispatch();
  const history = useHistory();
  
  const excluirProduto = async () => {
    const resultado = await excluirProdutoHelper( produtoExclusao );

    if ( resultado ) {
      dispatch(atualizarListaCompletaProdutos())
      history.push('/')
      } else {
        alert("Hove um erro!")
      }
    }


  return <>
    
     {categoriaSelecionada === NENHUMA_CATEGORIA_SELECIONADA ? null : <h4 className='pt-3'> Categora: {categoriaSelecionada}</h4>}
    
    <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-4 pt-3" id='lista-produtos'>
      
      {lista.map( item => (

     
        <div className="col">
        <div className="container">
          <Link to={`/produtos/${item.id}`}>
          <div className="card">
            <div className="imgBx">
              <img src={item.imagem} className="card-img-top" alt="Imagem do produto." />
            </div>
            <div className="contentBx">
              <h2 className= 'card-title'>{item.produto}</h2>
              <p className= 'card-text'>R$ {item.preco}</p>               
            </div>
          </div>
          </Link>
          { usuarioEstaLogado && usuarioLogado.tipo==="1" ?
           <div className= "col-12">
              <Link to={ `edicao-produto/${item.id}`}>
                <i className="fas fa-edit text-success"></i>
              </Link>
              <div onClick = { () => setProdutoExclusao(item.id)}>
                <i className="fas fa-trash-alt ms-3 text-danger" data-bs-toggle="modal" data-bs-target="#exampleModal"></i>
              </div>
            </div>
          : null }
          </div>

      </div>
      ))}
    </div>

    {/* Modal */}
    <div className= "modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div className= "modal-dialog modal-dialog-centered">
        <div className= "modal-content">
          <div className= "modal-header">
            <h5 className= "modal-title" id="exampleModalLabel">Exclusão de produto</h5>
            <button type="button" className= "btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div className= "modal-body">
            Você tem certeza que deseja excluir esse produto?
          </div>
          <div className= "modal-footer">
            {/* <button type="button" className= "btn btn-secondary" data-bs-dismiss="modal">Fechar</button> */}
            <button type="button" className= "btn btn-danger" data-bs-dismiss="modal" onClick ={excluirProduto}>Sim</button>
            <button type="button" className= "btn btn-secondary" data-bs-dismiss="modal">Não</button>
          </div>
        </div>
      </div>
    </div>

  </>;

};

export default ListaProdutos;